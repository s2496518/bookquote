package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String ibsn) {
        HashMap<String, Double> values = new HashMap<String, Double>();
        values.put("1", 10.0);
        values.put("2", 45.0);
        values.put("3", 20.0);
        values.put("4", 35.0);
        values.put("5", 50.0);
        if (values.get(ibsn) != null) {
            return values.get(ibsn);
        } else {
            return 0.0;
        }
    }
}